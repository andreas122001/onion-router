package no.ntnu.luka.node;

import no.ntnu.luka.lib.cryptography.AESCryptographer;
import no.ntnu.luka.lib.cryptography.AESEncrypter;
import no.ntnu.luka.lib.exceptions.ConnectionSetupException;
import no.ntnu.luka.lib.exceptions.EncrypterException;
import no.ntnu.luka.lib.exceptions.PacketReadException;
import no.ntnu.luka.lib.model.packet.ORPacket;
import no.ntnu.luka.lib.IO.ORPacketReader;
import no.ntnu.luka.lib.utils.KeyUtil;
import no.ntnu.luka.node.core.MasterConnector;
import no.ntnu.luka.node.core.NodeReceiveThread;
import no.ntnu.luka.node.core.NodeSendThread;

import javax.crypto.SecretKey;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * A node in the Luka-network. A server that helps to forward messages between a client and a host.
 *
 * @author Andreas B. Winje
 */
public class NodeServer {
    private final AESCryptographer crypto; // Contains secret key and encryption/decryption methods.

    private final int port; // server port.

    /**
     * Creates a node server-object. This is the normal use-case constructor.
     * @param port to run on.
     */
    public NodeServer(int port) {
        this.port = port;
        crypto = new AESCryptographer();
    }

    /**
     * Constructor that forces the node to run with a specific key (Base64-encoded key).
     * @param port to run on.
     * @param key secret key for encryption.
     */
    public NodeServer(int port, String key) {
        this.port = port;
        crypto = new AESCryptographer(KeyUtil.toSecretKey(key));
    }

    /**
     * Starts the node server.
     */
    public void start() {

        System.out.println("Running MasterConnector thread.");
        MasterConnector master = new MasterConnector(this);
        master.start();

        try (ServerSocket ss = new ServerSocket(port)) {
            System.out.println("Serving on port: " + port);
            while(!ss.isClosed()) {
                Socket socket = ss.accept();

                System.out.println(" * Client connected!");

                new Thread(() -> {
                    serve(socket);
                }).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up connection and serves it.
     * @param socket to serve (previous node).
     */
    public void serve(Socket socket) {
        try (socket) {
            Object[] socketAndPacket = setupConnectionToHost(socket);
            try (Socket nextSocket = (Socket) socketAndPacket[0]) {

                Thread send = new NodeSendThread(socket, nextSocket, getKey(),(ORPacket) socketAndPacket[1]);
                Thread receive = new NodeReceiveThread(nextSocket, socket, getKey(), (ORPacket) socketAndPacket[1]);

                send.start();
                receive.start();

                send.join();
                receive.join();

            } catch (InterruptedException e) {
                System.err.println("Thread exception: " + e.getMessage());
                e.printStackTrace();
            }
        } catch (ConnectionSetupException e) {
            System.err.println("Setup failed: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Network error: " + e.getMessage());
        }
        System.out.println(" * Client disconnected.");
    }

    /**
     * Reads data from previous socket and creates a new connection with next socket.
     * @param socket of the previous node.
     * @return socket of next node + packet header.
     * @throws ConnectionSetupException if setup fails.
     */
    public Object[] setupConnectionToHost(Socket socket) throws ConnectionSetupException {
        try {
            ORPacketReader inPrev = new ORPacketReader(socket.getInputStream());
            DataOutputStream outPrev = new DataOutputStream(socket.getOutputStream());

            ORPacket packet = inPrev.readFully(getKey());

            Socket nextSocket = new Socket(packet.getHeader().getIP().toString(), packet.getHeader().getPort());

            DataInputStream inNext = new DataInputStream(nextSocket.getInputStream());
            DataOutputStream outNext = new DataOutputStream(nextSocket.getOutputStream());

            if (packet.getHeader().getFIN() == 0) {
                outNext.write(packet.getData());

                int size = inNext.readInt();
                byte[] ack = inNext.readNBytes(size);
                ack = AESEncrypter.encryptToBytes(ack, getKey());

                outPrev.writeInt(ack.length);
                outPrev.write(ack);

            } else {
                byte[] ack = AESEncrypter.encryptToBytes(new byte[]{69}, getKey());
                outPrev.writeInt(ack.length);
                outPrev.write(ack);
            }

            System.out.println("Connection acknowledged.");

            return new Object[]{nextSocket, packet};

        } catch (IOException e) {
            throw new ConnectionSetupException("I/O-exception");
        } catch (PacketReadException e) {
            throw new ConnectionSetupException("Packet unreadable");
        } catch (EncrypterException e) {
            throw new ConnectionSetupException("Failed to encrypt ACK-response");
        }
    }

    public int getPort() {
        return port;
    }

    public SecretKey getKey() {
        return crypto.getKey();
    }
}
