package no.ntnu.luka.node.core;

import no.ntnu.luka.lib.Configuration;
import no.ntnu.luka.lib.model.ORNodeModel;
import no.ntnu.luka.lib.model.server.RequestObject;
import no.ntnu.luka.node.NodeServer;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * A thread that keeps a connection to the master-server to be discoverable by clients.
 *
 * @author Andreas B. Winje
 */
public class MasterConnector extends Thread {

    private final NodeServer server;

    public MasterConnector(NodeServer server) {
        this.server = server;
        this.setName("MasterConnector");
    }

    @Override
    public void run() {
        System.out.println("["+getName()+"] " + "Connecting to master...");

        while (true) {
            try (Socket master = new Socket(Configuration.MASTER_IP, Configuration.MASTER_PORT)) {
                try (master) {

                    ObjectOutputStream out = new ObjectOutputStream(master.getOutputStream());
                    // The ip-address will be changed at the master-server automatically, that's why its 0.0.0.0
                    ORNodeModel node = new ORNodeModel("0.0.0.0", server.getPort(), server.getKey());
                    RequestObject request = new RequestObject(node);

                    out.writeObject(request);

                    System.out.println("["+getName()+"] " + "Master connected!");

                    while (!master.isClosed())
                        out.write(1);

                } catch (IOException e) {
                    System.err.println("["+getName()+"] " + "Lost connection to master! You are no longer discoverable by clients.");
                }
            } catch (IOException e) {
                System.err.println("["+getName()+"] " + "Could not connect to master: " + e.getMessage());
            }
            try {
                System.out.println("["+getName()+"] " + "Retrying in 10 seconds.");
                Thread.sleep(10000);
                System.out.println("["+getName()+"] " + "Retrying connection...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
