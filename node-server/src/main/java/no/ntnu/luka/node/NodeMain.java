package no.ntnu.luka.node;

/**
 * Main-class for NodeServer
 */
public class NodeMain {
    public static void main(String[] args) {
        if (args.length == 1)
            new NodeServer(Integer.parseInt(args[0])).start();
        else new NodeServer(8085).start();
    }
}
