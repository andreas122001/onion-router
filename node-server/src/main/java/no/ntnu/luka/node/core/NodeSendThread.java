package no.ntnu.luka.node.core;

import no.ntnu.luka.lib.IO.BufferedCryptoReader;
import no.ntnu.luka.lib.IO.BufferedCryptoWriter;
import no.ntnu.luka.lib.exceptions.DecrypterException;
import no.ntnu.luka.lib.model.packet.ORPacket;
import no.ntnu.luka.lib.model.packet.ORPacketHeader;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.net.Socket;

/**
 * Thread for sending data to a server host.
 *
 * @author Andreas B. Winje
 */
public class NodeSendThread extends Thread {

    private final BufferedCryptoReader in;
    private final BufferedCryptoWriter out;
    private final SecretKey key;
    private final Socket receiver;
    private final Socket sender;
    private final ORPacketHeader header;

    public NodeSendThread(Socket receiver, Socket sender, SecretKey key, ORPacket packet) throws IOException {
        this.receiver = receiver;
        this.sender = sender;
        this.in = new BufferedCryptoReader(receiver.getInputStream());
        this.key = key;
        this.header = packet.getHeader();
        this.out = new BufferedCryptoWriter(sender.getOutputStream());

    }

    @Override
    public void run() {
        try (receiver; sender) {
            while (true) {
                // Reads data
                byte[] data = in.read(key);

                System.out.println("[Sender]: " + new String(data));
                // If I am last node: talk to host
                if (header.getFIN() == 1) out.writeToHost(data);
                else out.write(data);
            }
        } catch (IOException | DecrypterException | NullPointerException ignore) {}
    }

}
