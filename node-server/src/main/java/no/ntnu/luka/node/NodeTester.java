package no.ntnu.luka.node;

/**
 * Test-class that runs three node servers with given keys.
 */
public class NodeTester {
    public static void main(String[] args) {
        new Thread(() -> new NodeServer(8085, "m0TEMZK5f9lXg2zL21dcfA==").start()).start();
        new Thread(() -> new NodeServer(8086, "wzLRbefo0ZUYnBZDf1vdog==").start()).start();
        new Thread(() -> new NodeServer(8087, "imSndo5KZoJJU8wGG/BPqg==").start()).start();
    }
}
