package no.ntnu.luka.node.core;

import no.ntnu.luka.lib.IO.BufferedCryptoReader;
import no.ntnu.luka.lib.IO.BufferedCryptoWriter;
import no.ntnu.luka.lib.exceptions.EncrypterException;
import no.ntnu.luka.lib.model.packet.ORPacket;
import no.ntnu.luka.lib.model.packet.ORPacketHeader;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.net.Socket;

/**
 * Thread for receiving data from a server host.
 *
 * @author Andreas B. Winje
 */
public class NodeReceiveThread extends Thread {

    private final BufferedCryptoWriter out;
    private final BufferedCryptoReader in;
    private final SecretKey key;
    private final Socket receiver;
    private final Socket sender;
    private final ORPacketHeader header;

    public NodeReceiveThread(Socket receiver, Socket sender, SecretKey key, ORPacket packet) throws IOException {
        this.receiver = receiver;
        this.sender = sender;
        this.out = new BufferedCryptoWriter(sender.getOutputStream());
        this.key = key;
        this.header = packet.getHeader();
        this.in = new BufferedCryptoReader(receiver.getInputStream());
    }

    @Override
    public void run() {
        try (receiver; sender) {
            while (true) {
                // Read data
                byte[] data;
                // if I am last node: talk to host
                if (header.getFIN() == 1) data = in.readFromHost();
                else data = in.read();

                System.out.println("[Receiver]: " + new String(data));

                // Sends it to previous node
                out.write(data, key);
            }
        } catch (IOException | EncrypterException | NullPointerException ignore) {}
    }

}
