package no.ntnu.luka.lib.model.server;

import java.io.Serializable;

/**
 * The mode of a master-server request.
 * USER = from client.
 * NODE = from node-server.
 *
 * @author Andreas B. Winje
 */
public enum UserMode implements Serializable {
    USER, NODE
}
