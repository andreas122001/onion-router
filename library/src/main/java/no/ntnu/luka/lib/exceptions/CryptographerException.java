package no.ntnu.luka.lib.exceptions;

/**
 * Just a wrapper for the many cryptography-exceptions. Just... too many...
 *
 * @author Andreas B. Winje
 */
public class CryptographerException extends Exception {
    public CryptographerException() {
    }
    public CryptographerException(String message) {
        super(message);
    }
    public CryptographerException(String message, Throwable cause) {
        super(message, cause);
    }
}
