package no.ntnu.luka.lib.cryptography;

import no.ntnu.luka.lib.exceptions.CryptographerException;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Just a test-class. Nothing to seee...
 */
public class CryptoTesting {
    public static void main(String[] args) throws NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, InvalidKeyException, CryptographerException {

        String expectedFullyDecrypted = "Enc1\nEnc2\nEnc3";

        AESCryptographer encrypter = new AESCryptographer();
        AESCryptographer encrypter2 = new AESCryptographer();
        AESCryptographer encrypter3 = new AESCryptographer();
        AESCryptographer decrypter = new AESCryptographer(encrypter.getKey());
        AESCryptographer decrypter2 = new AESCryptographer(encrypter2.getKey());
        AESCryptographer decrypter3 = new AESCryptographer(encrypter3.getKey());

        String message = "Enc3";

        // Layered encryption
        message = encrypter3.encryptToBase64(message);
        message = "\nEnc2".concat(message);
        message = encrypter2.encryptToBase64(message);
        message = "\nEnc1".concat(message);
        message = encrypter.encryptToBase64(message);

        message = decrypter.decryptToString(message);
//        assertTrue(message.contains("Enc1"));

        System.out.println(message);

        message = decrypter2.decryptToString(message);
//        assertTrue(message.contains("Enc2"));

        System.out.println(message);





    }


}
