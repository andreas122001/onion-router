package no.ntnu.luka.lib.utils;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/**
 * Utility class to simplify dealing with SecretKey. Methods are self-explanatory...
 *
 * @author Andreas B. Winje
 */
public class KeyUtil {

    public static String toBase64(SecretKey key) {
        return Base64.getEncoder().encodeToString(key.getEncoded());
    }

    public static byte[] toBytes(SecretKey key) {
        return key.getEncoded();
    }

    public static SecretKey toSecretKey(String encodedBase64) {
        byte[] decodedKey = Base64.getDecoder().decode(encodedBase64);
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }

    public static SecretKey toSecretKey(byte[] encodedBytes) {
        return new SecretKeySpec(encodedBytes, 0, encodedBytes.length, "AES");
    }
}
