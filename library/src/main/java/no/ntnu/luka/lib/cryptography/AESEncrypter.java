package no.ntnu.luka.lib.cryptography;

import no.ntnu.luka.lib.exceptions.EncrypterException;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * A utility-class for encrypting data. The method-names explain themselves...
 *
 * @author Andreas B. Winje
 */
public class AESEncrypter {

    public static final String algorithm = "AES";

    public static byte[] encryptToBytes(byte[] message, SecretKey key) throws EncrypterException {
        try {
            Cipher encryptCipher = Cipher.getInstance(algorithm);
            encryptCipher.init(Cipher.ENCRYPT_MODE, key);
            return encryptCipher.doFinal(message);
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException e) {
            throw new EncrypterException("Error",e);
        }
    }

    public static byte[] encryptToBytes(String message, SecretKey key) throws EncrypterException {
        try {
            Cipher encryptCipher = Cipher.getInstance(algorithm);
            encryptCipher.init(Cipher.ENCRYPT_MODE, key);
            return encryptCipher.doFinal(message.getBytes());
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException e) {
            throw new EncrypterException("Error",e);
        }
    }

    public static String encryptToString(String encryptedBase64, SecretKey key) throws EncrypterException {
        return Base64.getEncoder().encodeToString(encryptToBytes(encryptedBase64, key));
    }

    public static String encryptToString(byte[] message, SecretKey key) throws EncrypterException {
        return Base64.getEncoder().encodeToString(encryptToBytes(message, key));
    }




}
