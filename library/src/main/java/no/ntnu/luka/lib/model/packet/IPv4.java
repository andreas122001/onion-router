package no.ntnu.luka.lib.model.packet;

/**
 * Encapsulates a IPv4-address as a 4-byte array.
 *
 * @author Andreas B. Winje
 */
public class IPv4 {

    private final int num1;
    private final int num2;
    private final int num3;
    private final int num4;

    public IPv4(String ip) throws NumberFormatException{
        String[] parts = ip.split("\\.");
        this.num1 = Integer.parseInt(parts[0]);
        this.num2 = Integer.parseInt(parts[1]);
        this.num3 = Integer.parseInt(parts[2]);
        this.num4 = Integer.parseInt(parts[3]);
    }

    public IPv4(byte[] bytes) {
        this.num1 = bytes[0]&0xff;
        this.num2 = bytes[1]&0xff;
        this.num3 = bytes[2]&0xff;
        this.num4 = bytes[3]&0xff;
    }

    public IPv4(int num1, int num2, int num3, int num4) {
        this.num1 = (byte) num1;
        this.num2 = (byte) num2;
        this.num3 = (byte) num3;
        this.num4 = (byte) num4;
    }

    public String toString() {
        byte[] address = getAddressBytes();
        StringBuilder s = new StringBuilder((address[0]&0xff) + "");
        for (int i = 1; i < 4; i++) {
            s.append(".").append(address[i]&0xff);
        }
        return s.toString();
    }

    public byte[] getAddressBytes() {
        return new byte[]{(byte) num1, (byte) num2, (byte) num3, (byte) num4};
    }
}
