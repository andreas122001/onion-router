package no.ntnu.luka.lib.IO;

import no.ntnu.luka.lib.cryptography.AESDecrypter;
import no.ntnu.luka.lib.exceptions.DecrypterException;

import javax.crypto.SecretKey;
import java.io.*;
import java.util.ArrayList;

/**
 * Simplifies reading between client, nodes and host. Methods include reading and decrypting data,
 * reading raw data and reading from a server host.
 *
 * Some methods are marked as DEPRECATED, but are kept here for future development.
 * The reader is not optimal as it should deal with fixed size messages, and not variable size like it
 * does now (for security).
 *
 * In the future, this class will be refactored to reflect these weaknesses using some kind of buffer - hence the name
 * BufferedCryptoReader.
 *
 * @author Andreas B. Winje
 */
public class BufferedCryptoReader {

    private final DataInputStream in; // input stream
    public static int CHUNK_SIZE = 15; // IGNORE: unused

    public BufferedCryptoReader(InputStream in) {
        this.in = new DataInputStream(in);
    }

    /**
     * Reads a line from the host.
     * @return data.
     * @throws IOException I/O-error.
     */
    public byte[] readFromHost() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        return reader.readLine().getBytes();
    }

    /**
     * Reads data going to the client.
     * @return data.
     * @throws IOException I/O-error.
     */
    public byte[] read() throws IOException {
        int size = in.readInt();
        return in.readNBytes(size);
    }

    /**
     * Read data coming from the client.
     * @param key to decrypt with.
     * @return data.
     * @throws IOException I/O-error.
     * @throws DecrypterException decryption-error.
     */
    public byte[] read(SecretKey key) throws IOException, DecrypterException {

        int size = in.readInt();
        byte[] data = in.readNBytes(size);

        return AESDecrypter.decryptToBytes(data, key);
    }

    /**
     * Used by client to read and decrypt data from the network.
     * @param keys to decrypt with.
     * @return data.
     * @throws DecrypterException decryption error.
     * @throws IOException I/O-error.
     */
    public byte[] read(SecretKey[] keys) throws DecrypterException, IOException {
        int size = in.readInt();

        byte[] data = in.readNBytes(size);
        for (SecretKey key : keys) {
            data = AESDecrypter.decryptToBytes(data, key);
        }
        return data;
    }

    public void close() throws IOException {
        in.close();
    }


    /** DEPRECATED
     *
     * @param key
     * @return
     * @throws IOException
     * @throws DecrypterException
     */
    public byte[] readBuffered(SecretKey key) throws IOException, DecrypterException {
        byte[] data = in.readNBytes(512);
        byte[] decrypted = AESDecrypter.decryptToBytes(data, key);

        byte[] padded = new byte[512];
        for (int i = 0; i < decrypted.length; i++) {
            padded[i] = decrypted[i];
        }

        return padded;
    }

    /** DEPRECATED
     * Reads data as encrypted chunks of size CHUNK_SIZE
     * @param key
     * @return
     * @throws IOException
     * @throws DecrypterException
     */
    public byte[] readAndDecrypt(SecretKey key) throws IOException, DecrypterException {
        boolean EOF = false;
        ArrayList<Byte> data = new ArrayList<>(CHUNK_SIZE);

        while (!EOF) {
            byte[] buffer = in.readNBytes(CHUNK_SIZE+1);
            if (buffer.length == 0) break;
            byte[] decrypted = AESDecrypter.decryptToBytes(buffer, key);

            for (int i = 0; i < decrypted.length; i++) {
                if (decrypted[i] == 0) {
                    EOF = true;
                    break;
                }
                data.add(decrypted[i]);
            }
        }

        byte[] dataArray = new byte[data.size()];
        for (int i = 0; i < data.size(); i++)
            dataArray[i] = data.get(i);

        return dataArray;
    }



}
