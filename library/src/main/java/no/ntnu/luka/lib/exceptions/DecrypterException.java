package no.ntnu.luka.lib.exceptions;

/**
 * For when the decrypter fails...
 *
 * @author Andreas B. Winje
 */
public class DecrypterException extends CryptographerException {
    public DecrypterException() {
    }
    public DecrypterException(String message) {
        super(message);
    }
    public DecrypterException(String message, Throwable cause) {
        super(message, cause);
    }
}
