package no.ntnu.luka.lib.exceptions;

/**
 * Problems with connection to master-server.
 *
 * @author Andreas B. Winje
 */
public class MasterConnectionException extends Exception {
    public MasterConnectionException() {
    }
    public MasterConnectionException(String message) {
        super(message);
    }
    public MasterConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
