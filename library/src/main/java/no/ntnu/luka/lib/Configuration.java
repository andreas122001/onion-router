package no.ntnu.luka.lib;

/**
 * Contains application-configuration.
 * This is a temporary fix.
 *
 * @author Andreas B. Winje
 */
public class Configuration {

    public static final String MASTER_IP = "lukarouter.tk";
    public static final int MASTER_PORT = 8099;

}
