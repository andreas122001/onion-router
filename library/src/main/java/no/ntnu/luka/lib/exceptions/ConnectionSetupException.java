package no.ntnu.luka.lib.exceptions;

/**
 * @author Andreas B. Winje
 */
public class ConnectionSetupException extends Exception {
    public ConnectionSetupException() {
    }
    public ConnectionSetupException(String message) {
        super(message);
    }
    public ConnectionSetupException(String message, Throwable cause) {
        super(message, cause);
    }
}
