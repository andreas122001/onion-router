package no.ntnu.luka.lib.utils;

import java.util.Arrays;

/**
 * A utility-class to deal with bits for working with data-headers.
 * originally this class was created in the WebSocket-assignment, and worked on through this project.
 *
 * @author Andreas B. Winje
 */
public class BitUtil {

    public static void main(String[] args) {

        int pos = 0;
        int num = 2;

        int i = 0b10000000000000001000101010101010;

        int shift = i>>>(32-(pos+num));


    }

    public static int combineBytesToInt(byte[] bytes) {
        int size = bytes.length * 8;
        int[] bits = new int[size];
        int index = 0;
        for (byte b : bytes) {
            for (int i = 0; i < 8; i++) {
                bits[index] = getBit(b,i);
                index++;
            }
        }
        return (int) buildLong(bits);
    }

    /**
     * Builds a number from an integer-array of bits.
     * The array must have the specified size for different enforcing number-types.
     * @param bits to build with.
     * @param size of the number-type in bits.
     * @return a byte using the bits.
     */
    public static long buildLong(int[] bits, int size) {
        if (bits.length != size)
            throw new IllegalArgumentException("Does not equal specified length.");
        if (!Arrays.stream(bits).allMatch(b -> b == 1 || b == 0))
            throw new IllegalArgumentException("Bits can only contain 0 and/or 0");
        long long_ = 0;
        for (int b : bits) {
            if (b == 0) {
                long_ = (long_<< 1);
            } else if (b == 1) {
                long_ = ((long_<<1) + 1);
            }
        }
        return long_;
    }

    /**
     * Same as above, but does not need to specify size.
     * @param bits to build with.
     * @return number.
     */
    public static long buildLong(int[] bits) {
        return buildLong(bits, bits.length);
    }

    /**
     * Builds an integer from an array of bits.
     * Array must have length of 32.
     * @param bits to build with.
     * @return an integer build by the bits.
     */
    public static int buildInt(int[] bits) {
        return (int) buildLong(bits, 32);
    }

    /**
     * Builds a byte from an integer-array of bits.
     * The array must have a length of 8 and can only contain the digits 0 or 1.
     * @param bits to build with.
     * @return a byte using the bits.
     */
    public static byte buildByte(int[] bits) {
        return (byte) buildLong(bits, 8);
    }

    /**
     * Returns a bit from a byte at {@code position} from left to right.
     * @param data to get from.
     * @param position of bit.
     * @return bit at position in data.
     */
    public static byte getBit(byte data, int position) {
        return getBits(data, position, 1);
    }

    /**
     * Returns {@code num} bits from {@code data} (an integer) from {@code offset} from left to right.
     * @param data to get from.
     * @param offset from the left.
     * @param num number of bits to get.
     * @return byte containing the bits from {@code data}.
     */
    public static byte getBitsFromInt(int data, int offset, int num) {
        // Shift byte
        int shifted = (data>>>32-(num+offset));
        // Mask byte
        int masked = (int) (Math.pow(2,num)-1);
        return (byte) (shifted & masked);
    }

    /**
     * Returns {@code num} bits from {@code data} from {@code offset} from left to right.
     * @param data to get from.
     * @param offset from the left.
     * @param num number of bits to get.
     * @return byte containing the bits from {@code data}.
     */
    public static byte getBits(byte data, int offset, int num) {
        // Shift byte
        int shifted = (data>>>8-(num+offset));
        // Mask byte
        int masked = (int) (Math.pow(2,num)-1);
        return (byte) (shifted & masked);
    }

    /**
     * Prints a given byte as a string of ones and zeroes.
     * E.g. printByte((byte) 10) prints the string "00001010".
     * @param b byte to print.
     */
    public static void printByte(int b) {
        for (int i = 0; i < 8; i++) {
            System.out.print(b>>(7-i)&0b1);
        }
        System.out.println();
    }
}
