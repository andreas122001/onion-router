package no.ntnu.luka.lib.cryptography;

import no.ntnu.luka.lib.exceptions.CryptographerException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * A class to simplify encryption and decryption with AES.
 *
 * @author Andreas B. Winje
 */
public class AESCryptographer {

    // TODO: add integrity check

    private final static String algorithm = "AES";
    private final SecretKey key;
    private final int keySize;
    private IvParameterSpec iv;

    /**
     * Creates a standard Cryptographer-object.
     */
    public AESCryptographer() {
        this(128);
    }

    /**
     * Creates a Cryptographer-object given a key for decryption
     * @param key decryption key.
     */
    public AESCryptographer(SecretKey key) {
        this.key = key;
        this.keySize = key.getEncoded().length;
    }

    /**
     * Creates a Cryptographer-object given a key and an initialization vector for decryption
     * @param key decryption key.
     * @param iv initialization vector.
     */
    public AESCryptographer(SecretKey key, IvParameterSpec iv) {
        this.key = key;
        this.iv = iv;
        this.keySize = key.getEncoded().length;
    }

    /**
     * Creates a Cryptographer-object by generating a key and initialization vector given a keySize.
     * @param keySize size of key.
     */
    public AESCryptographer(int keySize) {
        this.keySize = keySize;
        KeyGenerator keyGenerator = null;
        try {
            keyGenerator = KeyGenerator.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        keyGenerator.init(keySize);
        this.key = keyGenerator.generateKey();
        this.iv = generateIv();
    }

    // Getter
    public SecretKey getKey() {
        return key;
    }

    // Getter
    public int getKeySize() {
        return keySize;
    }

    // Getter
    public IvParameterSpec getIv() {
        return iv;
    }

    /**
     * Encrypts a message to a byte-array.
     * @param message to be encrypted.
     * @return encrypted bytes.
     */
    public byte[] encryptToBytes(String message) throws CryptographerException {
        return AESEncrypter.encryptToBytes(message,key);
    }

    /**
     * Encrypts a message to a byte-array.
     * @param message to be encrypted.
     * @return encrypted bytes.
     */
    public byte[] encryptToBytes(byte[] message) throws CryptographerException {
        return AESEncrypter.encryptToBytes(message,key);
    }

    /**
     * Encrypts a message to a base64-encoded string.
     * @param message to be encrypted.
     * @return
     */
    public String encryptToBase64(String message) throws CryptographerException {
        return AESEncrypter.encryptToString(message,key);
    }

    /**
     * Encrypts a message to a base64-encoded string.
     * @param message to be encrypted.
     * @return encrypted text in base64-encoding.
     */
    public String encryptToBase64(byte[] message) throws CryptographerException {
        return AESEncrypter.encryptToString(message,key);
    }

    /**
     * Decrypts a message to a byte-array.
     * @param encryptedBase64 to be decrypted.
     * @return decrypted bytes.
     */
    public byte[] decryptToBytes(String encryptedBase64) throws CryptographerException {
        return AESDecrypter.decryptToBytes(encryptedBase64,key);
    }

    /**
     * Decrypts a message to a byte-array.
     * @param encryptedBytes to be decrypted.
     * @return decrypted bytes.
     */
    public byte[] decryptToBytes(byte[] encryptedBytes) throws CryptographerException {
        return AESDecrypter.decryptToBytes(encryptedBytes, key);
    }

    /**
     * Decrypts a message to a string.
     * @param encryptedBase64 to be decrypted.
     * @return decrypted text.
     */
    public String decryptToString(String encryptedBase64) throws CryptographerException {
        return AESDecrypter.decryptToString(encryptedBase64, key);
    }

    /**
     * Decrypts a message to a string.
     * @param encryptedBytes to be decrypted.
     * @return decrypted text.
     */
    public String decryptToString(byte[] encryptedBytes) throws CryptographerException {
        return AESDecrypter.decryptToString(encryptedBytes,key);
    }

    /**
     * Generates an initialization vector.
     * @return initialization vector.
     */
    public IvParameterSpec generateIv() {
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        return new IvParameterSpec(iv);
    }
}
