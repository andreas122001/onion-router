package no.ntnu.luka.lib;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This is just an echo-server for testing.
 * Nothing to see here....
 */
public class ExampleServer {

    public static void main(String[] args) throws IOException {

        try (ServerSocket ss = new ServerSocket(8090)) {
            System.out.println("Listening on: " + 8090);
            while (true) {
                Socket socket = ss.accept();
                System.out.println("Connection at " + socket.getPort());

                new Thread(() -> serve(socket)).start();
            }
        }
    }

    public static void serve(Socket socket) {
        try (socket) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            String data;
            while ((data = reader.readLine()) != null) {

                System.out.println(socket.getPort()+": "+ data);

                writer.println(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
