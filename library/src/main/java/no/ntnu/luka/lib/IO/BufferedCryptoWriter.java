package no.ntnu.luka.lib.IO;

import no.ntnu.luka.lib.cryptography.AESEncrypter;
import no.ntnu.luka.lib.exceptions.EncrypterException;

import javax.crypto.SecretKey;
import java.io.*;

/**
 * Simplifies writing between client, nodes and host.
 * This class has methods for writing encrypted messages, passing messages without encrypting,
 * and for communicating with a server host (e.g. google.com).
 *
 * Some methods are marked as DEPRECATED, but are kept here for future development.
 * The writer is not optimal as it should deal with fixed size messages, and not variable size like it
 * does now (for security).
 *
 * In the future, this class will be refactored to reflect these weaknesses using some kind of buffer - hence the name
 * BufferedCryptoWriter.
 *
 * @author Andreas B. Winje
 */
public class BufferedCryptoWriter {

    private final DataOutputStream out;
    public static int CHUNK_SIZE = 15; // IGNORE: Currently unused

    public BufferedCryptoWriter(OutputStream out) {
        this.out = new DataOutputStream(out);
    }

    /**
     * Writes a message to the host, without zero-filling and size int.
     * @param data to write.
     */
    public void writeToHost(byte[] data) {
        PrintWriter writer = new PrintWriter(out, true);
        writer.println(new String(data));
    }

    /**
     * Writes data going to the client.
     * @param data to write.
     * @param key to encrypt with.
     */
    public void write(byte[] data, SecretKey key) throws EncrypterException, IOException {
        byte[] enc = AESEncrypter.encryptToBytes(data,key);
        out.writeInt(enc.length);
        out.write(enc);
    }

    /**
     * Writes data going from the client.
     * @param data to write.
     */
    public void write(byte[] data) throws IOException {
        out.writeInt(data.length);
        out.write(data);
    }

    /**
     * Writes encrypted data to the network.
     * @param data to write.
     * @param keys to encrypt with.
     */
    public void write(byte[] data, SecretKey[] keys) throws EncrypterException, IOException {
        byte[] encrypted = data.clone();
        for (int j = keys.length-1; j >=0; j--) { // encrypts block
            encrypted = AESEncrypter.encryptToBytes(encrypted, keys[j]);
        }
        out.writeInt(encrypted.length);
        out.write(encrypted);
    }

    public void close() throws IOException {
        out.close();
    }



    // IGNORE ALL UNDER:

    /** DEPRECATED - kept for future implementations.
     * Encrypts data into blocks of 512-bytes and sends it.
     * Data that is smaller than 512-16*numberOfEncryptions bytes will be zero-filled.
     * Data that is larger than this number will be sent in a new block and zero-filled.
     * @param data to send.
     * @param keys to encrypt with.
     * @throws EncrypterException encryption exception.
     * @throws IOException I/O error.
     */
    public void writeFixed(byte[] data, SecretKey[] keys) throws EncrypterException, IOException {

        int contentLength = 512 - 16 * keys.length; // possible size of content.
        int pos = 0;
        for (int i = 0; i < (data.length/contentLength) + 1; i++) { // for each 512-byte block.
            byte[] encrypted = new byte[contentLength];         // block to be encrypted.
            for (int j = 0; j < encrypted.length; j++) {
                if (pos == data.length) break;
                encrypted[j] = data[pos];
                pos++;
            }

            for (int j = keys.length-1; j >=0; j--) { // encrypts block
                encrypted = AESEncrypter.encryptToBytes(encrypted, keys[j]);
            }

            System.out.println("(SENDING): " + encrypted.length);
            out.writeInt(encrypted.length);
            System.out.println("(SENDING): " + new String(encrypted));
            out.write(encrypted);
        }
    }

    /** DEPRECATED
     * Writes data as encrypted buffers of size CHUNK_SIZE.
     * @param data
     * @throws EncrypterException
     * @throws IOException
     */
    public void writeAndEncryptBuffered(byte[] data, SecretKey key) throws EncrypterException, IOException {

        int rest = data.length % CHUNK_SIZE;
        int size = data.length - rest;

        // Write chunks of 16 (encrypted)
        byte[] buffer = new byte[CHUNK_SIZE];
        int pos;
        for (pos = 0; pos < size;) {
            for (int j = 0; j < buffer.length; j++) {
                buffer[j] = data[pos];
                pos++;
            }
            out.write(AESEncrypter.encryptToBytes(buffer, key));
        }

        // Write the last buffer (zero-filled and encrypted)
        buffer = new byte[CHUNK_SIZE];
        int j = 0;
        for (int k = 0; k < rest; k++) {
            int test = (data.length)-rest+j;
            buffer[j] = data[test];
            j++;
        }

        out.write(AESEncrypter.encryptToBytes(buffer, key));
    }


}
