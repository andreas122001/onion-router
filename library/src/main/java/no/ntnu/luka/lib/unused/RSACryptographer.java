package no.ntnu.luka.lib.unused;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;

/**
 * Same as AESCryptographer, but with RSA (for asymmetric-encryption).
 * Might be used in the future.
 * @author Andreas B. Winje
 */
public class RSACryptographer {

    private final BigInteger publicKey;
    private final BigInteger encryprionKey;
    private final BigInteger decryptionKey;

    public static void main(String[] args) {
        RSACryptographer cryptographer = new RSACryptographer();
        BigInteger[] encrypted = cryptographer.encrypt("Hello", cryptographer.getPublicKey());

        System.out.println(Arrays.toString(encrypted));

//        long startTime = System.currentTimeMillis();
//
//        RSACryptographer a = new RSACryptographer();
//        RSACryptographer b = new RSACryptographer();
//        RSACryptographer c = new RSACryptographer();
//
//        String textA = "hello, B";
//        String textB = "Hello, C";
//
//        BigInteger[] encryptedMessageToB = a.encrypt(textA,b.publicKey);
//        BigInteger[] encryptedMessageToC = a.encrypt(textB,c.publicKey);
//
//        System.out.println("A to B: " + b.decrypt(encryptedMessageToB));
//        System.out.println("A to C: " + c.decrypt(encryptedMessageToC));
//        System.out.println("C read A to B" + c.decrypt(encryptedMessageToB));
//
//        long endTime = System.currentTimeMillis();
//        long timeElapsed = endTime - startTime;
//        System.out.println("Time: " + timeElapsed);
    }

    public String toString() {
        return "Public key: " + publicKey;
    }

    public RSACryptographer() {
        BigInteger privateKeyA = BigInteger.probablePrime(160, new Random());
        BigInteger privateKeyB = BigInteger.probablePrime(160, new Random());
        this.publicKey = privateKeyA.multiply(privateKeyB);
        BigInteger totient = (privateKeyA.subtract(BigInteger.ONE)).multiply(privateKeyB.subtract(BigInteger.ONE));
        this.encryprionKey = BigInteger.valueOf(65537); // 65537
        this.decryptionKey = modInverse(encryprionKey, totient);
    }

    public BigInteger getPublicKey() {
        return publicKey;
    }

    public BigInteger[] encrypt(String text, BigInteger publicKey) {
        BigInteger[] encryptedBytes = new BigInteger[text.length()];
        byte[][] byteArrays = new byte[text.length()][];

        byte[] bytes = text.getBytes();
        for (int i = 0; i < bytes.length; i++) {
            BigInteger m = BigInteger.valueOf(bytes[i]);
            encryptedBytes[i] = m.modPow(encryprionKey,publicKey);
            byteArrays[i] = m.toByteArray();
        }

        return encryptedBytes;
    }

    public String decrypt(BigInteger[] c) {
        StringBuilder decryptedString = new StringBuilder();
        for (BigInteger bigInteger : c) {
            BigInteger m = bigInteger.modPow(decryptionKey, publicKey);
            decryptedString.append(new String(m.toByteArray()));
        }

        return decryptedString.toString();
    }

    private BigInteger modInverse(BigInteger a, BigInteger m) {
        return a.modInverse(m);
    }


}
