package no.ntnu.luka.lib.model.packet;

/**
 * Wrapper-class for encrypted header and data. Kinda not really needed, but other classes depend on it now so...
 * And it's just easier this way.
 *
 * @author Andreas B. Winje
 */
public final class OREncryptedPacket {

    private final byte[] header;
    private final byte[] data;

    public OREncryptedPacket(byte[] header, byte[] data) {
        this.header = header;
        this.data = data;
    }

    public byte[] getHeader() {
        return header;
    }

    public byte[] getData() {
        return data;
    }

    public byte[] toBytes() {
        byte[] bytes = new byte[header.length + data.length];

        int i = 0;
        for (byte b : header) {
            bytes[i] = b; i++;
        }

        for (byte b : data) {
            bytes[i] = b; i++;
        }


        return bytes;
    }
}
