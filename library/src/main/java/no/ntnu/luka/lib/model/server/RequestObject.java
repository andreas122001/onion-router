package no.ntnu.luka.lib.model.server;

import no.ntnu.luka.lib.model.ORNodeModel;

import java.io.Serializable;

/**
 * Encapsulates a request to the master-server. This class is serializable for using with Object-Reader/Writer
 *
 * @author Andreas B. Winje
 */
public class RequestObject implements Serializable {

    private ORNodeModel node; // the node (node-mode)
    private int nodeAmount; // how many nodes (client-mode)
    private final UserMode mode; // if it is a node-server or a client.

    /**
     * Constructor to be used by node-server. Requests a register of itself to the master-server.
     * @param node information about the node (server-port, crypto-key, etc.).
     */
    public RequestObject(ORNodeModel node) {
        this.node = node;
        this.mode = UserMode.NODE;
    }

    /**
     * Constructor to be used by client - requesting an amount of node.
     * @param maxNodeAmount maximum amount of nodes to receive.
     */
    public RequestObject(int maxNodeAmount) {
        this.nodeAmount = Math.max(maxNodeAmount, 1);
        this.mode = UserMode.USER;
    }

    public ORNodeModel getNode() {
        return node;
    }

    public int getNodeAmount() {
        return nodeAmount;
    }

    public UserMode getMode() {
        return mode;
    }
}
