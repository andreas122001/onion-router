package no.ntnu.luka.lib.model.packet;

import no.ntnu.luka.lib.utils.BitUtil;

/**
 * This class encapsulates a header in a LUKA-packet. The header has encrypted information about the next node,
 * which the current node will use to connect to it.
 * The header is constructed by the client in layers of encryption, and parsed as a byte-array by each node.
 *
 * The class is a bit of a mess right now - but it shouldn't be too hard to fix, and it works, so why bother.
 *
 * The header consists of 4 1-bit flags:
 * - FIN: only on if the current node is the last node in the network.
 * - UDF1 - UDF3: Unused Protocol Flag (not used right now, but serves as reserve space in case they are needed in the future).
 *
 * It also has some other fields like:
 * - POS: 4-bit, tells the position of this node to the host-server (not really used in the protocol).
 * - length: 1 byte, length of data.
 * - port: 2 bytes, port number of next node.
 * - ip: 4 bytes, ip-address of next node.
 *
 * @author Andreas B. Winje
 */
public class ORPacketHeader {

    private final int FIN;       // 1
    private final int UDF1;      // 1
    private final int UPF2;      // 1
    private final int UPF3;      // 1
    private final int POS;       // 4
    private final int length;    // 8
    private final int port;      // 16
    private final IPv4 ip;       // 32

    /**
     * Creates a header with given values.
     * @param FIN finish?
     * @param POS position.
     * @param length data length.
     * @param port node port.
     * @param ip node ip.
     */
    public ORPacketHeader(int FIN, int POS, int length, int port, String ip) {
        this.FIN = FIN;
        this.UDF1 = 0;
        this.UPF2 = 0;
        this.UPF3 = 0;
        this.POS = POS;
        this.length = length;
        this.port = port;
        this.ip = new IPv4(ip);
    }

    /**
     * Creates a header with given values.
     * @param FIN is finished
     * @param UPF1 unused
     * @param UPF2 unused
     * @param UPF3 unused
     * @param POS position from host.
     * @param length data length.
     * @param port node port
     * @param ip node ip
     */
    public ORPacketHeader(int FIN, int UPF1, int UPF2, int UPF3, int POS, int length, int port, String ip) {
        this.FIN = FIN;
        this.UDF1 = UPF1;
        this.UPF2 = UPF2;
        this.UPF3 = UPF3;
        this.POS = POS;
        this.length = length;
        this.port = port;
        this.ip = new IPv4(ip);
    }

    public String toString() {
        return "Flags: "+FIN+""+ UDF1 +""+""+UPF2+""+UPF3 +"\n" +
                "Integrity: " + POS + "\n" +
                "Length: " + length + "\n" +
                "NXT Port: " + port + "\n" +
                "NXT IP: " + ip;
    }

    /**
     * Creates a byte-array of the header, ready to be sent.
     * @return a byte-array with header data.
     */
    public byte[] toBytes() {
        byte[] bytes = new byte[12];
        byte[] ipBytes = ip.getAddressBytes();

        bytes[0] = BitUtil.buildByte(new int[]{
                getFIN(), getUDF1(), getUPF2(), getUPF3(), BitUtil.getBit((byte) getPOS(), 4),BitUtil.getBit((byte) getPOS(), 5),
                BitUtil.getBit((byte) getPOS(), 6),BitUtil.getBit((byte) getPOS(), 7)
        });
        bytes[1] = 0;
        bytes[2] = BitUtil.getBitsFromInt(port,16,8);
        bytes[3] = BitUtil.getBitsFromInt(port,24,8);
        bytes[4] = ipBytes[0];
        bytes[5] = ipBytes[1];
        bytes[6] = ipBytes[2];
        bytes[7] = ipBytes[3];
        bytes[8]  = BitUtil.getBitsFromInt(length,0,8);
        bytes[9]  = BitUtil.getBitsFromInt(length,8,8);
        bytes[10] = BitUtil.getBitsFromInt(length,16,8);
        bytes[11] = BitUtil.getBitsFromInt(length,24,8);

        return bytes;
    }


    // TODO: more checking...
    /**
     * Constructs a ORPacketHeader from a byte-array.
     * @param data to turn into ORPacketHeader.
     * @return A ORPacketHeader.
     */
    public static ORPacketHeader parseData(byte[] data) {
        if (data.length < 12)
            throw new IllegalArgumentException("Packet is too small");
        byte byte1 = data[0];
        byte byte2 = data[1];
        byte byte3 = data[2];
        byte byte4 = data[3];
        byte byte5 = data[4];
        byte byte6 = data[5];
        byte byte7 = data[6];
        byte byte8 = data[7];
        byte byte9 = data[8];
        byte byte10 = data[9];
        byte byte11 = data[10];
        byte byte12 = data[11];

        return new ORPacketHeader(
                BitUtil.getBit(byte1, 0),
                BitUtil.getBit(byte1, 1),
                BitUtil.getBit(byte1, 2),
                BitUtil.getBit(byte1, 3),
                BitUtil.getBits(byte1,4,4),
                BitUtil.combineBytesToInt(new byte[]{byte9, byte10, byte11, byte12}),
                BitUtil.combineBytesToInt(new byte[]{ byte3, byte4 }),
                (byte5&0xff)+"."+(byte6&0xff)+"."+(byte7&0xff)+"."+(byte8&0xff)
        );
    }

    public int getFIN() {
        return FIN;
    }

    public int getUDF1() {
        return UDF1;
    }

    public int getUPF2() {
        return UPF2;
    }

    public int getUPF3() {
        return UPF3;
    }

    public int getPOS() {
        return POS;
    }

    public int getLength() {
        return length;
    }

    public int getPort() {
        return port;
    }

    public IPv4 getIP() {
        return ip;
    }


}
