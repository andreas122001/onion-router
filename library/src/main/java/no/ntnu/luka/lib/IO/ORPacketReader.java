package no.ntnu.luka.lib.IO;

import no.ntnu.luka.lib.cryptography.AESDecrypter;
import no.ntnu.luka.lib.exceptions.PacketReadException;
import no.ntnu.luka.lib.exceptions.DecrypterException;
import no.ntnu.luka.lib.model.packet.ORPacket;
import no.ntnu.luka.lib.model.packet.ORPacketHeader;

import javax.crypto.SecretKey;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A InputReader that simplifies reading Luka-packets. Will also decrypt the message.
 * This is only used for connection-setup in both the client and the nodes.
 *
 * @author Andreas B. Winje
 */
public class ORPacketReader {

    private final DataInputStream in; // input stream

    public ORPacketReader(InputStream in) {
        this.in = new DataInputStream(in);
    }

    public ORPacketReader(DataInputStream in) {
        this.in = in;
    }

    /**
     * Fully reads and decrypts a LUKA-packet. Used while setting up connection between client and host.
     * @return packet as ORPacket object.
     * @throws PacketReadException if reading fails.
     */
    public ORPacket readFully(SecretKey key) throws PacketReadException {
        try {
            // Reads first 16 bytes into an integer (= length of encrypted message)
            int length = 16; // STANDARD 16 BYTE

            byte[] encryptedHeader = new byte[length];
            int debug = in.readNBytes(encryptedHeader, 0, length);

            // Decrypts the header
            ORPacketHeader header = ORPacketHeader.parseData(
                    AESDecrypter.decryptToBytes(encryptedHeader, key));

            // Read the data
            int dataLength = header.getLength();

            byte[] encryptedData = new byte[dataLength];
            debug = in.readNBytes(encryptedData,0,dataLength);

            // Decrypt the data
            byte[] data = AESDecrypter.decryptToBytes(encryptedData, key);

            return new ORPacket(header, data);

        } catch (IOException | DecrypterException e) {
            throw new PacketReadException(e.getMessage());
        }
    }

    /**
     * Closes the stream. (Think sockets does this automatically?)
     */
    public void close() throws IOException {
        in.close();
    }
}
