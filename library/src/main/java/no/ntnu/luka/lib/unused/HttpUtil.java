package no.ntnu.luka.lib.unused;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * Would deal with http - but is unused per now.
 * @author Andreas B. Winje
 */
public class HttpUtil {

    /**
     * Reads a http-header from a DataInputStream.
     * This method is taken from earlier project "WebSocketServer".
     * @param in datastream.
     * @return http-header as hash-map.
     * @throws IOException at I/O-error.
     */
    public static HashMap<String, String> getResponseHeader(DataInputStream in) throws IOException {
        String headerLine = "";
        HashMap<String, String> header = new HashMap<>();
        char c;
        boolean prevNewLine = false;
        while (((c = (char)in.readByte()) != '\u0000')) {
            headerLine += c;
            if (c == '\n') {
                if (!prevNewLine) {
                    System.out.print(headerLine);
                    String[] headerLineSplit = headerLine.split(": ");
                    if (headerLineSplit.length == 2)
                        header.put(headerLineSplit[0].trim(), headerLineSplit[1].replace("\n","").trim());
                    else header.put("REQUEST", headerLine);
                    headerLine = "";
                } else {
                    break;
                }
                prevNewLine = true;
            } else if (c != '\r') prevNewLine = false;
        }
        return header;
    }
}
