package no.ntnu.luka.lib.model.server;

import no.ntnu.luka.lib.model.ORNodeModel;

import java.io.Serializable;

/**
 * Encapsulates a response from the master-server. Serializable to be used with Object-Reader/Writer.
 * Currently only has response for client - Node knows if it fails as the socket will close anyway.
 * So basically, encapsulates an array of ORNodeModels for the client.
 *
 * @author Andreas B. Winje
 */
public class ResponseObject implements Serializable {

    private final ORNodeModel[] nodes;

    public ResponseObject(ORNodeModel[] nodes) {
        this.nodes = nodes;
    }

    public ORNodeModel[] getNodes() {
        return nodes;
    }
}
