package no.ntnu.luka.lib.exceptions;

/**
 * For connection problems with the luka-socket.
 *
 * @author Andreas B. Winje
 */
public class LukaSocketException extends Exception {
    public LukaSocketException() {
    }
    public LukaSocketException(String message) {
        super(message);
    }
    public LukaSocketException(String message, Throwable cause) {
        super(message, cause);
    }
}
