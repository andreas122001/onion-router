package no.ntnu.luka.lib.exceptions;

/**
 * For when a ORPacketRead fails to read.
 *
 * @author Andreas B. Winje
 */
public class PacketReadException extends Exception {
    public PacketReadException() {
    }
    public PacketReadException(String message) {
        super(message);
    }
    public PacketReadException(String message, Throwable cause) {
        super(message, cause);
    }
}
