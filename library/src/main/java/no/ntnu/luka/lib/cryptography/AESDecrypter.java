package no.ntnu.luka.lib.cryptography;

import no.ntnu.luka.lib.exceptions.DecrypterException;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * A utility-class for decrypting data. The method-names explain themselves...
 *
 * @author Andreas B. Winje
 */
public class AESDecrypter {

    public final static String algorithm = "AES";

    public static byte[] decryptToBytes(byte[] encryptedBytes, SecretKey key) throws DecrypterException {
        try {
            Cipher decryptCipher = Cipher.getInstance(algorithm);
            decryptCipher.init(Cipher.DECRYPT_MODE, key);
            return decryptCipher.doFinal(encryptedBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                IllegalBlockSizeException | BadPaddingException e) {
            throw new DecrypterException("There was an error",e);
        }
    }

    public static byte[] decryptToBytes(String encryptedBase64, SecretKey key) throws DecrypterException {
        try {
            Cipher decryptCipher = Cipher.getInstance(algorithm);
            decryptCipher.init(Cipher.DECRYPT_MODE, key);
            return decryptCipher.doFinal(Base64.getDecoder().decode(encryptedBase64));
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException e) {
            throw new DecrypterException("Error",e);
        }
    }

    public static String decryptToString(String encryptedBase64, SecretKey key) throws DecrypterException {
        return new String(decryptToBytes(encryptedBase64, key));
    }

    public static String decryptToString(byte[] encryptedBytes, SecretKey key) throws DecrypterException {
        return new String(decryptToBytes(encryptedBytes, key));
    }

}
