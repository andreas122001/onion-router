package no.ntnu.luka.lib.model;

import javax.crypto.SecretKey;
import java.io.Serializable;

/**
 * A model (or wrapper) of a node-server with IP-address, server-port and crypto-key.
 *
 * @author Andreas B. Winje
 */
public class ORNodeModel implements Serializable {

    private String IPv4;
    private int port;
    private SecretKey key;

    public ORNodeModel(String IPv4, int port, SecretKey key) {
        this.IPv4 = IPv4;
        this.port = port;
        this.key = key;
    }

    public String toString() {
        return IPv4 + ":" + port;
    }

    public String getIPv4() {
        return IPv4;
    }

    public void setIPv4(String IPv4) {
        this.IPv4 = IPv4;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public SecretKey getKey() {
        return key;
    }

    public void setKey(SecretKey key) {
        this.key = key;
    }
}
