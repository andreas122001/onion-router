package no.ntnu.luka.lib.model.server;

/**
 * Determines what type the server response is.
 *
 * @author Andreas B. Winje
 */
public enum ResponseType {
    INFO, ERROR
}
