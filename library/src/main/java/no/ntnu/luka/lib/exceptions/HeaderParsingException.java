package no.ntnu.luka.lib.exceptions;

/**
 * Thrown when the ORPacketReader fails to read a ORHeader.
 * Not used...
 *
 * @author Andreas B. Winje
 */
public class HeaderParsingException extends Exception {
    public HeaderParsingException() {
    }
    public HeaderParsingException(String message) {
        super(message);
    }
    public HeaderParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
