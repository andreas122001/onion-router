package no.ntnu.luka.lib.model.packet;

/**
 * A model of a LUKA-packet which will contain info about all the nodes in a connection.
 * Separated in a header and data field. ORPacketHeader is another class encapsulating a header.
 *
 * @author Andreas B. Winje
 */
public class ORPacket {

    private ORPacketHeader header;
    private byte[] data;

    public ORPacket(ORPacketHeader header, byte[] data) {
        this.header = header;
        this.data = data;
    }

    public String toString() {
        return header.toString() + "\nData: " + new String(data);
    }

    public ORPacketHeader getHeader() {
        return header;
    }

    public byte[] toBytes() {
        byte[] headerBytes = header.toBytes();
        byte[] dataBytes = data;
        byte[] bytes = new byte[dataBytes.length + headerBytes.length];

        int i = 0;
        for (byte b : headerBytes) {
            bytes[i] = b;
            i++;
        }

        for (byte b : dataBytes) {
            bytes[i] = b;
            i++;
        }

        return bytes;
    }

    public byte[] getData() {
        return data;
    }
}
