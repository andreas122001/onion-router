package no.ntnu.luka.lib.exceptions;

/**
 * For when the encrypter fails...
 *
 * @author Andreas B. Winje
 */
public class EncrypterException extends CryptographerException {
    public EncrypterException() {
    }
    public EncrypterException(String message) {
        super(message);
    }
    public EncrypterException(String message, Throwable cause) {
        super(message, cause);
    }
}
