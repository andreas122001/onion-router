package no.ntnu.luka.lib;

import no.ntnu.luka.lib.IO.ORPacketReader;
import no.ntnu.luka.lib.cryptography.AESCryptographer;
import no.ntnu.luka.lib.cryptography.AESEncrypter;
import no.ntnu.luka.lib.exceptions.CryptographerException;
import no.ntnu.luka.lib.exceptions.PacketReadException;
import no.ntnu.luka.lib.model.packet.OREncryptedPacket;
import no.ntnu.luka.lib.model.packet.ORPacket;
import no.ntnu.luka.lib.model.packet.ORPacketHeader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ORPacketReaderTest {

    private AESCryptographer cryptographer = new AESCryptographer();
    private byte[] exampleData = "Hello!".getBytes();
    private ORPacketHeader exampleHeader;
    private ORPacketReader reader;
    private OREncryptedPacket encryptedPacket;

    @BeforeEach
    public void initialize() throws CryptographerException {
        byte[] exampleDataEncrypted = AESEncrypter.encryptToBytes(exampleData, cryptographer.getKey());
        exampleHeader = new ORPacketHeader(1,0,0,0,9,exampleDataEncrypted.length, 9090, "192.168.0.110");

        encryptedPacket = new OREncryptedPacket(
                AESEncrypter.encryptToBytes(exampleHeader.toBytes(), cryptographer.getKey()),
                exampleDataEncrypted
        );

        InputStream stub = new ByteArrayInputStream(encryptedPacket.toBytes());
        reader = new ORPacketReader(stub);
    }

    @AfterEach
    public void close() throws IOException {
        reader.close();
    }

    @Test
    public void testOfReadingEncryptedPacket() throws PacketReadException {
        ORPacket testPacket = reader.readFully(cryptographer.getKey());
        assertEquals(testPacket.getHeader().toString(), exampleHeader.toString());
        assertEquals(new String(testPacket.getData()), new String(exampleData));
    }
}
