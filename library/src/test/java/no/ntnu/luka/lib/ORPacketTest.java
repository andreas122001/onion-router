package no.ntnu.luka.lib;

import no.ntnu.luka.lib.model.packet.ORPacketHeader;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ORPacketTest {

    private ORPacketHeader examplePacket
            = new ORPacketHeader(1,0,0,0,9,6,9090,"10.0.0.1");

    @Test
    public void parseDataToPacketGivesExpectedPacket() {
        byte[] data = new byte[]{
                (byte) 0b10001001, // 1000 + 9
                (byte) 0b00000000, // 0
                (byte) 0b00100011, // port 1
                (byte) 0b10000010, // port 2   9090
                (byte) 0b00001010, // addr 1  10
                (byte) 0b00000000, // addr 2   0
                (byte) 0b00000000, // addr 3   0
                (byte) 0b00000001, // addr 4   1
                (byte) 0b00000000, // len 1
                (byte) 0b00000000, // len 2
                (byte) 0b00000000, // len 3
                (byte) 0b00000110, // len 4   6
        };

        ORPacketHeader testPacket = ORPacketHeader.parseData(data);

        assertEquals(testPacket.toString(), examplePacket.toString());
    }

    @Test
    public void toBytesThenParsingCreatesSamePacketContent() {
        byte[] data = examplePacket.toBytes();
        ORPacketHeader testPacket = ORPacketHeader.parseData(data);
        assertEquals(testPacket.toString(), examplePacket.toString());
    }
}
