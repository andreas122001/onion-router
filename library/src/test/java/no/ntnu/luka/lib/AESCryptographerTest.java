package no.ntnu.luka.lib;

import no.ntnu.luka.lib.cryptography.AESCryptographer;
import no.ntnu.luka.lib.exceptions.CryptographerException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class AESCryptographerTest {

    private AESCryptographer encrypter;
    private AESCryptographer decrypter;
    private final String message = "Hello!";

    @BeforeEach
    public void initialize() {
        encrypter = new AESCryptographer();
        decrypter = new AESCryptographer(encrypter.getKey());
    }

    @Test
    public void testOfLayeredEncryption() throws CryptographerException {
        String expectedFullyDecrypted = "Hello!";

        AESCryptographer encrypter2 = new AESCryptographer();
        AESCryptographer encrypter3 = new AESCryptographer();
        AESCryptographer decrypter2 = new AESCryptographer(encrypter2.getKey());
        AESCryptographer decrypter3 = new AESCryptographer(encrypter3.getKey());

        String message = "Hello!";

        // Layered encryption
        message = "Enc3".concat(message);
        message = encrypter3.encryptToBase64(message);
        message = "Enc2".concat(message);
        message = encrypter2.encryptToBase64(message);
        message = "Enc1".concat(message);
        message = encrypter.encryptToBase64(message);

        // Test is encrypted
        assertFalse(message.contains("Enc1"));
        assertFalse(message.contains("Enc2"));
        assertFalse(message.contains("Enc3"));

        System.out.println("Fully encrypted: " + message);

        message = decrypter.decryptToString(message);
        assertTrue(message.contains("Enc1"));
        assertFalse(message.contains("Enc2"));
        assertFalse(message.contains("Enc3"));
        System.out.println("Once decrypted: " + message);
        message = message.substring(4); // removes decrypted data

        message = decrypter2.decryptToString(message);
        assertFalse(message.contains("Enc1"));
        assertTrue(message.contains("Enc2"));
        assertFalse(message.contains("Enc3"));
        System.out.println("Twice decrypted: " + message);
        message = message.substring(4); // removes decrypted data

        message = decrypter3.decryptToString(message);
        assertFalse(message.contains("Enc1"));
        assertFalse(message.contains("Enc2"));
        assertTrue(message.contains("Enc3"));
        System.out.println("Fully decrypted: " + message);
        message = message.substring(4); // removes decrypted data

        System.out.println("Final message: " + message);

        assertEquals(message, expectedFullyDecrypted);
    }

    @Test
    public void encryptedMessagesDoesNotEqualMessage() throws CryptographerException {
        assertNotEquals(message, encrypter.encryptToBase64(message));
    }

    @Test
    public void decryptedTextEqualsOriginal() throws CryptographerException {
        String encrypted = encrypter.encryptToBase64(message);
        String decrypted = decrypter.decryptToString(encrypted);
        assertEquals(decrypted, message);
    }

    @Test
    public void encryptionDoesNotThrow() {
        assertDoesNotThrow(() -> {
            encrypter.encryptToBase64(message);
        });
        assertDoesNotThrow(() -> {
            encrypter.encryptToBytes(message);
        });
    }

    @Test
    public void decryptionDoesNotThrow() throws CryptographerException {
        String encrypted = encrypter.encryptToBase64(message);
        assertDoesNotThrow(() -> {
            decrypter.decryptToString(encrypted);
        });
        assertDoesNotThrow(() -> {
            decrypter.decryptToBytes(encrypted);
        });
    }

    @Test
    public void canDecryptToStringFromBytesAndOpposite() throws CryptographerException {
        byte[] encryptedBytes = encrypter.encryptToBytes(message);
        String decryptedString = decrypter.decryptToString(encryptedBytes);
        assertEquals(decryptedString, message);
        String encryptedString = encrypter.encryptToBase64(message);
        byte[] decryptedBytes = decrypter.decryptToBytes(encryptedString);
        assertEquals(new String(decryptedBytes), message);
    }

    @Test
    public void canEncryptAndDecryptBytes() throws CryptographerException {
        byte[] encrypted = encrypter.encryptToBytes(message.getBytes());
        byte[] decrypted = decrypter.decryptToBytes(encrypted);
        assertEquals(new String(decrypted), message);
    }
}
