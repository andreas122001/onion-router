package no.ntnu.luka.lib;

import no.ntnu.luka.lib.unused.RSACryptographer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

public class RSACryptographerTest {

    RSACryptographer a;
    RSACryptographer b;
    RSACryptographer c;

    final String messageFromA = "Hello, from A";
    final String messageFromB = "Hello, from B";

    @BeforeEach
    public void generateKeys() {
        a = new RSACryptographer();
        b = new RSACryptographer();
        c = new RSACryptographer();
    }

    @Test
    public void B_can_read_from_A() {
        BigInteger[] to_B_from_A = a.encrypt(messageFromA, b.getPublicKey());
        String decryptedB = b.decrypt(to_B_from_A);
        assertEquals(messageFromA, decryptedB);
    }

    @Test
    public void C_can_NOT_read_from_A_to_B() {
        BigInteger[] to_B_from_A = a.encrypt(messageFromA, b.getPublicKey());
        String decryptedC = c.decrypt(to_B_from_A);
        assertNotEquals(messageFromA, decryptedC);
    }

    @Test
    public void A_can_read_from_B_but_C_can_not() {
        BigInteger[] to_A_from_B = b.encrypt(messageFromB, a.getPublicKey());
        String decryptedA = a.decrypt(to_A_from_B);
        assertEquals(messageFromB, decryptedA);
        String decryptedC = c.decrypt(to_A_from_B);
        assertNotEquals(messageFromB, decryptedC);
    }

    @Test
    public void A_and_C_can_read_from_B() {
        BigInteger[] to_A_from_B = b.encrypt(messageFromB, a.getPublicKey());
        BigInteger[] to_C_from_B = b.encrypt(messageFromB, c.getPublicKey());
        String decryptedA = a.decrypt(to_A_from_B);
        assertEquals(messageFromB, decryptedA);
        String decryptedC = c.decrypt(to_C_from_B);
        assertEquals(messageFromB, decryptedC);
    }






}
