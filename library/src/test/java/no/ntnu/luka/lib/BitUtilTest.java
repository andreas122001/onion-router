package no.ntnu.luka.lib;


import org.junit.jupiter.api.Test;

import static no.ntnu.luka.lib.utils.BitUtil.*;
import static org.junit.jupiter.api.Assertions.*;

public class BitUtilTest {

    private int exampleNumber = 0b10011001100110011001100110011001;
    private byte exampleByte = (byte) 0b10011001;
    private int testNumber;


    @Test
    public void getBitReturnsExpectedBit() {
        testNumber = getBit(exampleByte, 0);
        assertEquals(testNumber, 1);
        testNumber = getBit(exampleByte, 7);
        assertEquals(testNumber, 1);
        testNumber = getBit(exampleByte, 1);
        assertEquals(testNumber, 0);
    }

    @Test
    public void getBitsReturnsExpectedNumber() {
        testNumber = getBits(exampleByte, 0,4);
        assertEquals(testNumber, 0b1001);
        testNumber = getBits(exampleByte, 4, 4);
        assertEquals(testNumber, 0b1001);
        testNumber = getBits(exampleByte, 2,4);
        assertEquals(testNumber, 0b0110);

        testNumber = getBitsFromInt(exampleNumber, 24, 8);
        assertEquals(testNumber, (byte) 0b10011001);
    }

    @Test
    public void buildByteReturnsExpectedByte() {
        testNumber = buildByte(new int[]{1,0,0,1,1,0,0,1});
        assertEquals(testNumber,exampleByte);
    }


}
