package no.ntnu.luka.client;

import no.ntnu.luka.lib.model.packet.IPv4;
import no.ntnu.luka.client.core.ReadThread;

import java.io.IOException;
import java.util.Scanner;

/**
 * Implementation of a Luka-client.
 *
 * @author Andreas B. Winje
 */
public class ClientMain {
    public static void main(String[] args) {

        String usage = "LukaClient [host-address:port]";

        Scanner scanner = new Scanner(System.in);
        String input;

        String host = "142.250.74.14";
        int port = 80;

        System.out.println("LukaClient v1.0\n");

        if (args.length == 1) {
            try {
                host = (String) parse(args[0])[0];
                port = (int) parse(args[0])[1];
            } catch (IllegalArgumentException e) {
                System.out.println("Usage: " + usage);
                System.exit(-1);
            }
        } else {

            while (true) {
                System.out.println("Where do you want to connect?");
                System.out.print("[host_address:port]: ");

                try {
                    input = scanner.nextLine().trim();

                    host = (String) parse(input)[0];
                    port = (int) parse(input)[1];

                    break;

                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid input, try again.");
                }
            }
        }

        System.out.println("Connecting to "+host+":"+port);

        try (LukaSocket client = new LukaSocket(host, port)) {

            new ReadThread(client).start();

            while(true) {
                input = scanner.nextLine();
                if (client.isClosed()) {
                    break;
                }
                client.write(input);
            }
        } catch (IOException e) {
            System.err.println("Connection closed.");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static Object[] parse(String arg) {
        String host;
        int port;

        try {
            host = arg.split(":")[0];
            port = Integer.parseInt(arg.split(":")[1]);
            new IPv4(host);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }

        return new Object[]{host, port};
    }
}
