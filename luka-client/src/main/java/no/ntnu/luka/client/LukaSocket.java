package no.ntnu.luka.client;


import no.ntnu.luka.lib.Configuration;
import no.ntnu.luka.lib.IO.BufferedCryptoReader;
import no.ntnu.luka.lib.IO.BufferedCryptoWriter;
import no.ntnu.luka.lib.cryptography.AESDecrypter;
import no.ntnu.luka.lib.cryptography.AESEncrypter;
import no.ntnu.luka.lib.exceptions.DecrypterException;
import no.ntnu.luka.lib.exceptions.EncrypterException;
import no.ntnu.luka.lib.exceptions.LukaSocketException;
import no.ntnu.luka.lib.exceptions.MasterConnectionException;
import no.ntnu.luka.lib.model.packet.OREncryptedPacket;
import no.ntnu.luka.lib.model.ORNodeModel;
import no.ntnu.luka.lib.model.packet.ORPacketHeader;
import no.ntnu.luka.lib.model.server.RequestObject;
import no.ntnu.luka.lib.model.server.ResponseObject;
import no.ntnu.luka.lib.utils.KeyUtil;

import javax.crypto.SecretKey;
import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * This class is an implementation of the client-side of LUKA-onion routing. It serves as a socket that connects
 * to a host through a set of nodes in a network of nodes. All the data that is sent is encrypted in layers, giving the
 * client private and anonymous communication with a server.
 *
 * Upon construction of a LukaSocket-object, the class will set up a connection by contacting a central server, getting a
 * list of node-servers that are available, and then sending a message to one of these nodes with data telling the node
 * where to connect next - until the message reaches the host the client wanted to reach. As said, all data is encrypted in
 * layers - to the host and back - so the communication is private.
 *
 * The LukaSocket then communicates using a reader and writer.
 *
 * @author  Andreas B. Winje
 */
public class LukaSocket implements AutoCloseable {

    private SecretKey[] keys;
    private final BufferedCryptoWriter w; // writer
    private final BufferedCryptoReader r; // reader
    private final Socket node;
    private boolean isClosed = false;

    public LukaSocket(String host, int port) throws IOException, LukaSocketException {
        this.node = setupConnection(host, port);
        this.w = new BufferedCryptoWriter(node.getOutputStream());
        this.r = new BufferedCryptoReader(node.getInputStream());
    }

    /**
     * Writes an encrypted packet to the network.
     * @param data message to send.
     */
    public void write(String data) throws EncrypterException, IOException {
        w.write(data.getBytes(), keys);
    }

    /**
     * Reads an encrypted packet from the network.
     * @return packet.
     * @throws IOException I/O-error
     * @throws DecrypterException decryption exception.
     */
    public byte[] read() throws IOException, DecrypterException {
        return r.read(keys);
    }

    /**
     * Sets up connection with a host through multiple nodes in the network.
     * @param host host-ip.
     * @param port host port.
     * @return a socket connected to the first node.
     * @throws LukaSocketException if connection-setup fails.
     */
    public Socket setupConnection(String host, int port) throws LukaSocketException {
        Socket nodeSocket;

        System.out.println("Setting up connection...");

        try {
            // Get nodes from master.
            System.out.println("Contacting master...");

            ORNodeModel[] nodes = getNodesFromMaster();

            System.out.println("Got nodes from master.");

            // Update keys
            keys = new SecretKey[nodes.length];
            for (int i = 0; i < nodes.length; i++) {
                keys[i] = nodes[i].getKey();
            }

            System.out.println("Contacting nodes...");

            // Encrypt initial headers
            byte[] encryptedSYN = generateEncryptedSYN(host, port, nodes).toBytes();

            nodeSocket = new Socket(nodes[0].getIPv4(), nodes[0].getPort());
//            nodeSocket.setSoTimeout(5000);
            DataInputStream in = new DataInputStream(nodeSocket.getInputStream());
            DataOutputStream out = new DataOutputStream(nodeSocket.getOutputStream());

            out.write(encryptedSYN);

            // Reads ACK-number and decrypts it.
            int size = in.readInt();
            byte[] ackIn = in.readNBytes(size);

            int ack = decrypt(ackIn, nodes)[0];

            System.out.println("Node connection acknowledged.");

            // If ACK-number is not 69, communication might be compromised, or something went wrong...
            if (ack != 69) {
                throw new LukaSocketException("Wrong ACK number ["+ack+"]. Communication could be compromised.\nAborting...");
            }

            System.out.println("Connection established!");

        } catch (EncrypterException | DecrypterException e) {
            throw new LukaSocketException("There was a problem with encrypting/decrypting: " + e.getMessage());
        } catch (MasterConnectionException e) {
            throw new LukaSocketException("Could not get nodes from master: " + e.getMessage());
        } catch (SocketTimeoutException e) {
            throw new LukaSocketException("Connection timed out. Host unreachable.");
        } catch (IOException e) {
            throw new LukaSocketException(e.getMessage());
        }

        return nodeSocket;
    }

    /**
     * Connects to master-server and gets a set of nodes.
     * @return nodes.
     * @throws MasterConnectionException if unable to connect or read from master or if there are no nodes available.
     */
    public ORNodeModel[] getNodesFromMaster() throws MasterConnectionException {
        try (Socket master = new Socket(Configuration.MASTER_IP, Configuration.MASTER_PORT)) {

            ObjectOutputStream out = new ObjectOutputStream(master.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(master.getInputStream());

            RequestObject request = new RequestObject(10); // will request max 10 nodes
            out.writeObject(request);

            ResponseObject response = (ResponseObject) in.readObject();
            ORNodeModel[] nodes = response.getNodes();

            if (nodes.length == 0) throw new MasterConnectionException("There are no nodes available to serve you... :( " +
                    "\nTry again later.");

            master.close();
            return nodes;

        } catch (IOException e) {
            throw new MasterConnectionException("Connectivity problems: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            throw new MasterConnectionException("Response is unreadable: " + e.getMessage());
        }
    }

    /**
     * Encrypts a LUKA-SYN-message.
     * @return encryptedMessage.
     */
    public OREncryptedPacket generateEncryptedSYN(String host, int port, ORNodeModel[] nodes) throws EncrypterException {

        ORNodeModel lastNode = nodes[nodes.length-1];
        SecretKey key = lastNode.getKey();

//        System.out.println("\nTo node: " +lastNode.getPort());
        byte[] encryptedData = new byte[0];//AESEncrypter.encryptToBytes(data, key);

//        System.out.println("Data length: " + encryptedData.length);
        ORPacketHeader header = new ORPacketHeader(1, nodes.length, encryptedData.length,port,host);
        OREncryptedPacket encryptedPacket = new OREncryptedPacket(
                AESEncrypter.encryptToBytes(header.toBytes(), key),
                encryptedData
        );
//        System.out.println("Header length: "+encryptedPacket.getHeader().length);
//        System.out.println(new String(encryptedData));

        for (int i = nodes.length-2; i >= 0; i--) {
//            System.out.println("\nTo node: " +nodes[i].getPort());
            key = nodes[i].getKey();
            ORNodeModel nextNode = nodes[i+1];

            // encrypt data and header into data:
            encryptedData = AESEncrypter.encryptToBytes(encryptedPacket.toBytes(), key);
            header = new ORPacketHeader(0, i+1,encryptedData.length, nextNode.getPort(), nextNode.getIPv4());
            encryptedPacket = new OREncryptedPacket(
                    AESEncrypter.encryptToBytes(header.toBytes(), key),
                    encryptedData
            );

//            System.out.println("Header length: "+encryptedPacket.getHeader().length);
//            System.out.println("Data length: " + encryptedData.length);
//            System.out.println("Encrypted header:\n"+ Arrays.toString(encryptedPacket.getHeader()));
//            System.out.println("Encrypted header:\n"+ new String(encryptedPacket.getHeader()));
//            System.out.println("Encrypted data:\n"+ Arrays.toString(encryptedPacket.getData()));
//            System.out.println("Encrypted data:\n"+ new String(encryptedPacket.getData()));

        }

        return encryptedPacket;
    }

    /**
     * Decrypts a message from a node.
     * Currently only used in synchronisation.
     * @param data to decrypt.
     * @param nodes encrypters.
     * @return decrypted message.
     * @throws DecrypterException if decryption fails.
     */
    public byte[] decrypt(byte[] data, ORNodeModel[] nodes) throws DecrypterException {
        SecretKey key;
        for (ORNodeModel node : nodes) {
            key = node.getKey();
            data = AESDecrypter.decryptToBytes(data, key);
        }
        return data;
    }

    /**
     * Creates three hardcoded nodes and keys for testing.
     * NodeServers must be set up with the same parameters.
     */
    public ORNodeModel[] mockNodes() {
        ORNodeModel[] nodes = new ORNodeModel[3];
        nodes[0] = new ORNodeModel("192.168.0.110", 8085, KeyUtil.toSecretKey("m0TEMZK5f9lXg2zL21dcfA=="));
        nodes[1] = new ORNodeModel("192.168.0.110", 8086, KeyUtil.toSecretKey("wzLRbefo0ZUYnBZDf1vdog=="));
        nodes[2] = new ORNodeModel("192.168.0.110", 8087, KeyUtil.toSecretKey("imSndo5KZoJJU8wGG/BPqg=="));
        return nodes;
    }

    @Override
    public void close() throws Exception {
        w.close();
        r.close();
        node.close();
        this.isClosed = true;
    }

    public boolean isClosed() {
        return isClosed;
    }
}
