package no.ntnu.luka.client.core;

import no.ntnu.luka.client.LukaSocket;

/**
 * Thread for reading from host.
 *
 * @author Andreas B. Winje
 */
public class ReadThread extends Thread {

    private final LukaSocket client;

    public ReadThread(LukaSocket client) {
        this.client = client;
    }

    @Override
    public void run() {
        try (client) {
            while (true)
                System.out.println("Received: " + new String(client.read()));
        } catch (Exception ignored) {}
        System.err.println("Connection closed.");
        System.exit(0);
    }
}
