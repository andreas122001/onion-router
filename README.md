# Frivillig prosjekt - Nettverksprogrammering

Medvirkende: **Andreas Bentzen Winje**

## Luka Router - An Onion Router Implementation

Her er noen nyttige linker:

Link til **hele prosjektet** på GitLab: https://gitlab.com/andreas122001/onion-router <br>
Link til **alle CI/CD-pipelines**: https://gitlab.com/andreas122001/onion-router/-/pipelines <br>
Link til **JavaDoc**: https://andreas122001.gitlab.io/onion-router/ <br>
Link til **.jar-filer (prod)**: https://gitlab.com/andreas122001/onion-router/-/jobs/artifacts/master/download?job=build%3Aall <br>
Link til **.jar-filer (localhost/lokal testing)**: https://gitlab.com/andreas122001/onion-router/-/jobs/artifacts/v1.0_localhost/download?job=build%3Aall <br>

Det kan hende at noen av linkene er utdaterte. Isåfall, gi beskjed til **andrebw@stud.ntnu.no** ved problemer.

## Introduksjon

En motivasjon for prosjektet var å lage en onion router som ikke var kjent av alle, som TOR er, slik at russere som blir blokkert av myndighetene kunne søke privat på nettet uten å bli brutt av (som ble sagt i forelesningen). I følge google translate skives 'løk' på russisk som 'luk', vi legger på en 'A' og vi får 'Luka'. Navnet på prosjektet er altså LukaRouter :)

LukaRouter er en løsning for onion-routing, implementert i java ved bruk av TCP-sockets. Trådmodellen som er brukt er én tråd per klient. Det er ikke brukt noen biblioteker og alt i prosjektet er rent java 11 (i tillegg er html/css/js brukt for å hoste JavaDoc, men det er egentlig ikke en del av selve prosjektet). Avhengigheter er kun brukt til testing og bygging av prosjektet.

Prosjektet er et mavenprosjekt, og består av tre deler: en klientmodul, en nodemodul og en servermodul. I tillegg har prosjektet et bibliotek som er en egen modul og alle de andre modulene avhenger av.

Prosjektet kommer i tillegg med en komplett CI/CD-løsning i GitLab, en ekstern linux tjener, som hoster løsningen, og et domenenavn, www.lukarouter.tk (det er foreløpig kun for api og har ikke en nettside). Det er laget enhetstester for det meste av bibliotek-delen.

Det er satt mye fokus på coupling og kohesjon i sammenheng med koden, og jeg vil derfor påstå at koden er enkel å endre på og videreutvikle.



## Virkemåte
Her er en mer in-depth virkemåte av alle modulene.

### LukaClient
Når klienten kjører vil den spørre om hvor den skal koble seg til, om det ikke er gitt i programkjøringsparametrene. Deretter vil den starte en LukaSocket, som først kobler seg på den sentrale tjeneren og spør om nodetjenere og deres krypteringsnøkler. Når Socketen har fått tak i nodene vil den generere en lagvis-kryptert melding med lagdelt informasjon om alle nodene og om sluttjeneren og sende denne til den første noden. Socketen vil da ta imot en kryptert ACK-melding som er krytpert av hver av nodene for så å dekryptere denne og sjekke at den stemmer. Den skal være lik tallet '69'. Om ACK-medlingen stemmer vil koblingen være satt opp melding klienten og sluttjeneren.

Nå kan klienten sende og motta meldinger fra tjeneren. For å sende meldinger krypterer klienten først meldingen med hver av nodenes krypteringsnøkkel og så sender den til den første noden.
Når klienten mottar noder vil den bruke alle krypteringsnøklene til å dekryptere meldingen, for så å vise den for bruker.

### NodeServer
NodeServeren starter med å starte en ny tråd som har i oppgave med å kontakte den sentrale serveren for å registrere seg og for å holde oppe denne koblingen.

Hovedtråden vil tjene innkommende klienter. Hver klient får da sin egen tråd som vil sette opp en kobling til neste node og så starte to nye tråder for lesing og skriving til hhv. forrige node/klient og neste node/sluttjener.

### MasterServer
MasterServer er den sentrale tjeneren som har i oppgave å holde styr på alle nodene i nettverket og å oppgi disse til eventuelle klienter. Tjeneren er en vanlig multithreaded-TCP-socket-tjener. Ved tilkobling vil den ta imot et java-object som vil fortelle om tilkoblingen er en node eller en klient. Om det er en node vil den lese informasjonen dens og sjekke om noden kan nås over internett. Om den kan nås over nettet vil den registrere noden i en liste og holde koblingen med den. Om tilkoblingen er av en klient vil tjeneren lese hvor mange noder klienten spør om for så å hente ut gitt antall noder tilfeldig fra lista og sende de tilbake. Tilkobling med klienten blir brutt etter responsen er sendt.


### Library
Biblioteket er en samling av kode som brukes av de andre modulene i prosjektet. Bibioteket sørger for at alle modulene bruker samme kode og klasser, noe som er med på å decouple koden.





## Implementert funksjonalitet

Her er en del funksjonalitet og implemetasjoner som er gjort ift. dette prosjektet.

- Lagvis AES-kryptering av meldinger.
- En klient som kan sende meldinger til en hvilken som helst server (e.g. google.com).
- En nodeserver for videreføring av meldinger til en server. En melding går gjennom mange slike nodeservere.
- Sentral server for registrering av nodeservere (MasterServer). Klient kontakter denne for å finne ut hvem som kan videreføre meldinger.
- Et bibliotek som gjør det enklere å programmere egne løsninger for LukaClient.
- Ekstern linux server (tar med dette siden det tok en del tid å konfigurere...).
- GitLab CICD med auto-deploy av MasterServer til ekstern Linux-server.
- JavaDoc hostet med GitLab Pages.
- Et domenenavn for den sentrale serveren (www.lukarouter.tk).



## Mangler, svakheter og framtidig arbeid

Omfanget av dette prosjektet er enormt, og en person eller en liten gruppe vil neppe klare å utvikle en fullstendig, stabil løsning på kort tid. Derfor vil det nødvendigvis være en del mangler og svakheter, men også mye motivasjon og inspirasjon for framtidig arbeid. Her er en del av svakhetene og manglene i dette prosjektet.

- **Krypteringen er en enkel type AES-kryptering (ECB)** som danner samme kryptering for samme blokk (og samme nøkkel). Jeg vet ikke hvordan man gjør noe annet i java og jeg valgte å ikke bruke så mye tid på det. Det burde ikke være noe problem siden det fortsatt er en del av AES-standarden, men det kan iallfall forbedres.


- **Meldinger sendes i variabel størrelse og ikke en 'fixed-size block'.** Dette gjør at en sniffer kan se på størrelsen av en melding og få et intrykk av hvor langt denne meldingen er kommet i node-nettverket. Dette kan fikses ved å 'zero-fille' meldingene som er mindre enn en gitt størrelse, og kutte meldinger som er over denne størelsen og så bare ignorere nullene i meldingen når den skal leses. Jeg har ikke funnet en god løsning på dette per nå, men slik koden er satt opp trengs det bare å endres i reader- og writerklassen i koden.


- **Klienten har ingen HTTPS-funksjonalitet**. Dette har jeg per nå ikke implementert, men det er definitivt en del av framtidig arbeid. Jeg er litt usikker på hvordan å implementere dette i koden som er, men det burde ikke være så vanskelig ettersom java har en del klasser for håndtering av dette.


- **Krypteringsnøkler opprettes per node-session og ikke per tilkobling**. Dvs. at når en nodeserver starter og så registrerer seg hos den sentrale serveren vil den beholde den samme krypteringsnøkkelen for hver klient for den samme kjøringen. Dette har sannsynligvis ingenting å si på dette stadiet, men det kan ses på som en svakhet i større sammenheng. Dette kan enkelt fikses ved å endre nøkkelen en gang i blant, eller bruke en egen nøkkel per klient, men det får bli en del av framtidig arbeid.


- **En nodeserver kan ikke tjene uten nettverkskonfigurering**. Dette er fordi en nodeserver sannsynligvis sitter bak en ruter og vil derfor ikke motta noen forespørsler uten at ruteren er satt til å sende trafikk til noden. Dette kan fikses ved å port-forwarde trafikken fra ruteren til nodeserveren, men dette må isåfall gjøres på hver enkelt ruter som har lyst til å tjene onion-routing nettverket. Dette kan muligens unngås  vha. NAT Hole Punching i selve programmet, eller lignende. Isåfall er det noe som kan jobbes med.


- **Ingen ordentlig UI**. Programmene mangler ordentlige brukergrensesnitt, og da tenker jeg mest på klientenprogrammet. Dette er noe jeg vil jobbe med og har noen idéer for. En idé er å lage en localhost-server for klienten som tjener requests fra en nettside via luka-nettverket. Man kan da enkelt besøke en nettside med en fin UI hostet av f.eks. www.lukarouter.tk mens man har et klienprogramm i bakgrunnen som tjener forespørsler fra nettsida. Eventuelt kan det kanskje utvikles en slags browser-extension som redirecter forespørsler til luka-nettverket.


- **Bare IPv4-adresser kan brukes**, domenenavn og IPv6 fungerer ikke. Dette er på grunn av implementasjonen av oppkoblingmetoden og pakke-headeren for luka-protokollen. I headeren er det satt av 4 byte for host-adressa som tilsvarer en ipv4-adresse. Om en prøver å sende et domenenavn eller ipv6 vil en få en feilmelding. Dette tror jeg egentlig er en ganske enkel fiks, men jeg har egentlig ikke turt å "ta" på koden som styrer oppkoblingen i frykt for å ødelegge noe. Den fungerer, og det tok lang tid for å få det til å fungere, så for å fikse på det tror jeg egentlig en må ha ganske god oversikt. Dette for være i framtida.



## Eksterne avhengigheter

I dette prosjektet er det ikke brukt noen eksterne avhengigheter untatt maven-dependencier for testing av java-klasser, "surefire" og "junit" og en maven bygge-plugin for å bygge .jar-filer som inneholder alle avhengigheter (i dette tilfellet, library-modulen som er en lokal-avhengighet) kalt "maven-shade-plugin".

Alt ellers i programmet er helt vanlig java, JDK 11.



## Installasjonsinstruksjoner

For å kunne kjøre noen av programmene må java være installert på maskina. Ellers trengs ingen annen installering. 

Programmene kjøres som .jar-filer og kan kjøres i et kommandovindu. De er shadet slik at de ikke trenger noen dependencies og skal kunne kjøres alene. Husk å kjør versjonen som har "-shaded.jar" på slutten, ikke den uten. 

1. For å kunne kjøre programmet må du ha .jar-filene. Om du ikke har fått de kan de lastes ned fra GitLab-siden. Enten fra seneste artifact eller releases.

2. For å kjøre et program, åpne et kommandovindu i samme katalog som .jar-fila og kjør følgende kommandoer for hhv. klient, node og server:

```
java -jar LukaClient-1.0-shaded.jar
java -jar NodeServer-1.0-shaded.jar
java -jar MasterServer-1.0-shaded.jar
```

Pass på at du er i samme katalog som hver av filene for å kjøre de. Om du får: 'Error: Unable to access jarfile *.jar', er filnavnet sannsynligvis feil, eller du er i feil katalog.


## Instruksjoner for å bruke løsningen

Bruk av løsningen forutsetter at alle delene av løsningen, (klient, node og server), kjører og kan nå hverandre over et nettverk. Om de kan det skal det ikke være værre enn å kjøre .jar-filene i et kommandovindu. 

Klientprogrammet kan kjøres med parametre som dette "[host]:[port]", eller uten. Uten parametre vil programmet spørre om parametre selv.

## Hvordan en kan kjøre tester

For å kunne testing av programmet med localhost-adresser har jeg laget en git-branch, '**v1.0-localhost**', med .jar-filer hvor alle adressene er satt til localhost. Link til .jar-filene for denne branchen finner du [her](https://gitlab.com/andreas122001/onion-router/-/jobs/artifacts/v1.0_localhost/download?job=build%3Aall). 

For å kunne teste ordentlig må en kjøre to eller flere noder. Pass på å bruk ulike portnumre for hver instans et program. Portnumre spesifiseres pak .jar-fila i kjørekommandoen.


