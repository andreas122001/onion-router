package no.ntnu.luka.master.exceptions;

public class ConnectionError extends Exception {
    public ConnectionError() {
    }

    public ConnectionError(String message) {
        super(message);
    }

    public ConnectionError(String message, Throwable cause) {
        super(message, cause);
    }
}
