package no.ntnu.luka.master.core;

import no.ntnu.luka.lib.model.ORNodeModel;
import no.ntnu.luka.lib.model.server.RequestObject;
import no.ntnu.luka.lib.model.server.ResponseObject;
import no.ntnu.luka.master.MasterServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static no.ntnu.luka.lib.model.server.UserMode.NODE;
import static no.ntnu.luka.lib.model.server.UserMode.USER;

public class UserThread extends Thread {

//    private final Logger LOGGER = Logger.getLogger(UserThread.class);

    private final Socket socket;
    private final MasterServer server;
    private ORNodeModel node;

    public UserThread(Socket socket, MasterServer server) {
        this.socket = socket;
        this.server = server;
    }

    @Override
    public void run() {
        try (socket) {

            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            RequestObject request = (RequestObject) in.readObject();

            if (request.getMode() == NODE) {

                node = new ORNodeModel(socket.getInetAddress().getHostAddress(),
                        request.getNode().getPort(), request.getNode().getKey());

                // Test if node is reachable.
//                try (Socket ignore = new Socket(node.getIPv4(), node.getPort())) {
//                } catch (IOException e) {
//                    throw new IOException("Node unreachable");
//                }

                server.addNode(node);
                System.out.println("* Node added! " + node.toString());

                while (!socket.isClosed())
                    socket.getInputStream().read();

            } else if (request.getMode() == USER) {

                System.out.println("* User requested nodes.");

                ORNodeModel[] nodes = server.getRandomNodes(request.getNodeAmount());

                ResponseObject response = new ResponseObject(nodes);

                out.writeObject(response);

                System.out.println("* User served!");
            }

        } catch (IOException e) {
            System.err.println("Lost connection to socket: " + e.getMessage());
            server.removeUser(this);
            if (server.removeNode(node))
                System.out.println("* Node removed!");
            else System.out.println("* User disconnected!");

        } catch (ClassNotFoundException e) {
            System.err.println("Unexpected message object: " + e.getMessage());
            server.removeUser(this);
            System.err.println("* User disconnected!");
        }
    }
}
