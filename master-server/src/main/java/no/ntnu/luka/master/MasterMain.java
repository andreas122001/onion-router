package no.ntnu.luka.master;

import no.ntnu.luka.lib.Configuration;
import no.ntnu.luka.master.exceptions.ServerException;

public class MasterMain {
    public static void main(String[] args) throws ServerException {
        if (args.length == 1)
            new MasterServer(Integer.parseInt(args[0]));
        else new MasterServer(Configuration.MASTER_PORT).start();
    }
}
