package no.ntnu.luka.master;

import no.ntnu.luka.lib.model.ORNodeModel;
import no.ntnu.luka.master.core.UserThread;

import no.ntnu.luka.master.exceptions.ServerException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;


public class MasterServer {

//    private final Logger LOGGER = Logger.getLogger(MasterServer.class);

    private final int port;
    private final Set<UserThread> users = new HashSet<>();
    private final Set<ORNodeModel> nodes = new HashSet<>();

    public MasterServer(int port) {
        this.port = port;
    }

    public void start() throws ServerException {
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println(" * Listening on port " + port);

            while (serverSocket.isBound() && !serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                System.out.println(" * Connection accepted: " + socket.getInetAddress());

                UserThread userThread = new UserThread(socket,this);

                addUser(userThread);

                userThread.start();

            }

        } catch (IOException e) {
            throw new ServerException("ServerSocket failed: " + e.getMessage());
        }
    }

    public ORNodeModel[] getRandomNodes(int maxAmount) {
        int amount = Math.min(maxAmount, nodes.size());
        ORNodeModel[] nodeArray = new ORNodeModel[amount];

        ArrayList<ORNodeModel> nodeCopy = new ArrayList<>(nodes);

        for (int i = 0; i < amount; i++) {
            int rnd = new Random().nextInt(nodeCopy.size());
            ORNodeModel node = nodeCopy.get(rnd);

            nodeArray[i] = node;
            nodeCopy.remove(node);
        }

        return nodeArray;
    }

    public boolean addNode(ORNodeModel node) {
        return nodes.add(node);
    }

    public boolean removeNode(ORNodeModel node) {
        return nodes.remove(node);
    }

    public boolean addUser(UserThread user) {
        return users.add(user);
    }

    public boolean removeUser(UserThread user) {
        return users.remove(user);
    }
}
